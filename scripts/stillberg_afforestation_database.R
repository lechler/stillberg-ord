# INFO ----

# This script is used to alter the Stillberg afforestation dataset in a way
# that makes it compatible for transformation into a database
# Author: Lia Lechler
# Starting Date: 23/02/2022

# SETTING UP WORKSPACE ----

# Load packages useful for data manipulation
library(tidyr)
library(dplyr)
library(stringr)
library(readr)

# Set working directory
setwd("C:/Git/stillberg-ord")

# Read .csv file from Mauro Marty's folder as the most complete Stillberg record
sb <- read.csv("outputs/afforestation/20230308_tree_parameters_wide_format.csv")
sa <- read.csv("outputs/afforestation/20230223_plot_parameters.csv")

# ASSESS DATA ----

# Check data import and data type
head(sb)
str(sb)

# Assess length of columns and rows
nrow(sb) # 91915
length(sb) # 46

# Check tree parameters
unique(sb$sample_trees_2005)
unique(sb$sample_trees_2015)
unique(sb$expm_trees)
unique(sb$death_year)
unique(sb$survival_2005)
unique(sb$survival_2015)
unique(sb$height_1975)
unique(sb$height_2005)
unique(sb$diameter_2010)
unique(sb$stem_2005)
length(which(sb$stem_2005 == "0")) # 4719
length(which(sb$stem_2005 == "1")) # 178
unique(sb$stem_2015)
length(which(sb$stem_2015 == "0")) # 5192
length(which(sb$stem_2015 == "1")) # 34
unique(sb$base_2005)
length(which(sb$base_2005 == "0"))
unique(sb$base_2015)
unique(sb$top_2005)
unique(sb$top_2015)
unique(sb$cone_2005)
unique(sb$cone_2015)
unique(sb$damage1_2005)
unique(sb$damage1_2015)
unique(sb$damage2_2005)
unique(sb$damage2_2015)
unique(sb$damage3_2005)
unique(sb$damage3_2015)
unique(sb$damage1_intensity_2005)
unique(sb$damage1_intensity_2015)
unique(sb$damage2_intensity_2005)
unique(sb$damage2_intensity_2015)
unique(sb$damage3_intensity_2005)
unique(sb$damage3_intensity_2015)
unique(sb$tree_covered_2015)
unique(sb$competition)

# Check plot parameters
unique(sa$rejuv_pinusc)
unique(sa$rejuv_larixd)
unique(sa$rhododendronf_perc)
unique(sa$plant_community)
unique(sa$soil_type)
unique(sa$humus_type)
unique(sa$location_type)
unique(sa$avalanche_barriers)
unique(sa$n_avalanches)


# MANIPULATE DATA ----

# Go through data set column by column and change values in column according to 
# encoding suitable for database

# 1) Tree parameters
# First old encoding in code, then new encoding
sb_1 <- sb %>% 
  mutate(sample_trees_2005 = recode(sample_trees_2005, "1" = "1", "0" = "-1"), 
         sample_trees_2015 = recode(sample_trees_2015, "1" = "1", "0" = "-1"),
         expm_trees = recode(expm_trees, "1" = "1", "2" = "2", "3" = "3", "0" = "4"),
         death_year = case_match(death_year, NA ~ -1, .default = death_year),         # case_match() requires dplyr 1.1.0
         survival_2005 = recode(survival_2005, "1" = "1", "0" = "100"),
         survival_2015 = recode(survival_2015, "1" = "1", "0" = "100"),
         height_1979 = case_match(height_1979, NA ~ -1, .default = height_1979),
         height_1982 = case_match(height_1982, NA ~ -1, .default = height_1982),
         height_1985 = case_match(height_1985, NA ~ -1, .default = height_1985),
         height_1990 = case_match(height_1990, NA ~ -1, .default = height_1990),
         height_1995 = case_match(height_1995, NA ~ -1, .default = height_1995),
         height_2005 = case_match(height_2005, NA ~ -1, .default = height_2005),
         height_2010 = case_match(height_2010, NA ~ -1, .default = height_2010),
         height_2015 = case_match(height_2015, NA ~ -1, .default = height_2015),
         perpend_height_2015 = case_match(perpend_height_2015, NA ~ -1, .default = perpend_height_2015),
         diameter_2010 = case_match(diameter_2010, NA ~ -1, .default = diameter_2010),
         diameter_2015 = case_match(diameter_2015, NA ~ -1, .default = diameter_2015),
         stem_2005 = case_match(stem_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 99, NA_integer_ ~ -1),
         stem_2015 = case_match(stem_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 99, NA_integer_ ~ -1),
         base_2005 = case_match(base_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, 0 ~ 100, NA_integer_ ~ -1),
         base_2015 = case_match(base_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, NA_integer_ ~ -1),
         top_2005 = case_match(top_2005, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1),
         top_2015 = case_match(top_2015, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1),
         cone_2005 = case_match(cone_2005, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1),
         cone_2015 = case_match(cone_2015, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1),
         damage1_2005 = case_match(damage1_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, 0 ~ 100, NA_integer_ ~ -1),
         damage1_2015 = case_match(damage1_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, 0 ~ 100, NA_integer_ ~ -1),
         damage2_2005 = case_match(damage2_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, 0 ~ 100, NA_integer_ ~ -1),
         damage2_2015 = case_match(damage2_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 4 ~ 4, 5 ~ 5, 0 ~ 100, NA_integer_ ~ -1),
         damage3_2005 = case_match(damage3_2005, 0 ~ 100, NA_integer_ ~ -1),
         damage3_2015 = case_match(damage3_2015, 1 ~ 1,  4 ~ 4, 5 ~ 5, 0 ~ 95, NA_integer_ ~ -1),
         damage1_intensity_2005 = case_match(damage1_intensity_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         damage1_intensity_2015 = case_match(damage1_intensity_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         damage2_intensity_2005 = case_match(damage2_intensity_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         damage2_intensity_2015 = case_match(damage2_intensity_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         damage3_intensity_2005 = case_match(damage3_intensity_2005, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         damage3_intensity_2015 = case_match(damage3_intensity_2015, 1 ~ 1, 2 ~ 2, 3 ~ 3, 0 ~ 100, NA_integer_ ~ -1),
         tree_covered_2015 = case_match(tree_covered_2015, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1),
         competition = case_match(competition, 1 ~ 1, 0 ~ 100, NA_integer_ ~ -1))


# EXPORT CSV FILE ----
# write.csv(sb_1, file = "outputs/afforestation/20230313_database_tree_parameters_wide_format.csv", row.names = FALSE)


# 2) Plot parameters
# First old encoding in code, then new encoding
#lets do this after the Q&A meeting with Mauro and Rolf!


