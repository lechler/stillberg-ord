Instructions for setting up Git and connecting it to R

Download Git here: https://git-scm.com/downloads
 
Great manual to get Git started: https://happygitwithr.com/hello-git.html

SSL keys: You'll probably be prompted to add an SSH key to your gitlabext account in a pop-up window on your screen when you open gitlabext.wsl.ch. This key code allows your machine to access the repository; to push and pull information and code from it to your machine. The key must be on your machine, and it also has to be uploaded to your gitlabext account.

If you already have an SSH key (stored under C:\Users\username\.ssh), you can add them to your gitlabext account in your profile "settings/SSH keys" and you should be able to clone into the repository. If this folder does not yet exist (which likely is the case), you will need to generate an SSH key. For this, you'll have to open your git bash (where you also "introduced yourself" to git before) and type: $ ssh-keygen -t ed25519 -C "your_email@example.com". This creates a new SSH key under C:\Users\username\.ssh.

Next, open the .pub file in this directory with a text editor, and copy paste the key code into your the big white field in your user settings/SSH keys on gitlabext.wsl.
If you successfully uploaded an SSH key to your gitlabext account, you pretty much made it and you should be able to copy paste the cloning link from your repository (blue button!) to R. To do so, open a new project,  choose the third option "use version control", paste the cloning link, and select a directory for the project of your choice. A window opens, and you might have to say "yes" again. This should successfully connect R with Git.

(More details on happygitwithr)
