Git Project Etiquette:

1) Keep file paths short and sensible.

2) Use all lower case letters and words separated with underscores for filenames.

3) Always pull before you start working on your project and before you push in case someone has done any work since the last time you pulled.
